# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime

# Create your models here.

class Garment(models.Model):
	name = models.CharField(max_length=20,default='EMPTY')
	cloth_path = models.CharField(max_length=5000,default='EMPTY')
	cloth_mask_path = models.CharField(max_length=5000,default='EMPTY')

	# raw_s3_link = models.CharField(max_length=5000,default='EMPTY')

	def __str__(self):
		return str(self.pk) + " - " + str(self.name)


class Pose(models.Model):
	name = models.CharField(max_length=20,default='EMPTY')
	image_path = models.CharField(max_length=5000,default='EMPTY')
	image_parse_path = models.CharField(max_length=5000,default='EMPTY')
	pose_path = models.CharField(max_length=5000,default='EMPTY')

	def __str__(self):
		return str(self.pk) + " - " + str(self.name)


class GOP(models.Model):
	garment = models.ForeignKey(Garment,on_delete=models.CASCADE)
	pose = models.ForeignKey(Pose,on_delete=models.CASCADE)
	warp_cloth_path = models.CharField(max_length=5000,default='EMPTY')
	warp_cloth_mask_path = models.CharField(max_length=5000,default='EMPTY')
	tryon_path = models.CharField(max_length=5000,default='EMPTY')
	start_time = models.DateTimeField(default=datetime.now)
	end_time= models.DateTimeField(default=datetime.now)
	def __str__(self):
		return str(self.pk) + " - " + str(self.garment.name) + " - " + str(self.pose.name)


class Setting(models.Model):
	is_running = models.BooleanField(default=False)
	pose = models.CharField(max_length=5000,default='EMPTY')
	garment = models.CharField(max_length=5000,default='EMPTY')
	def __str__(self):
		return str(self.pk) + " - " + str(self.garment) + " - " + str(self.pose)



