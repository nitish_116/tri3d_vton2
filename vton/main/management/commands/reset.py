from django.core.management import BaseCommand
from django.conf import settings

import os
from django.contrib.auth.models import User
from main.models import Garment,Pose,GOP,Setting

class Command(BaseCommand):
	# Show this when the user types help
	help = "selected for bulk flush"
	
	# A command must define handle()
	def handle(self, *args, **options):
		for cur_setting in Setting.objects.all():
			pass
		if not cur_setting:
			cur_setting = Setting()
			cur_setting.save()
		cur_setting.is_running = False
		cur_setting.save()

		