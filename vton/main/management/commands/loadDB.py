from django.core.management import BaseCommand
from django.conf import settings

import os
from django.contrib.auth.models import User
from main.models import Garment,Pose,GOP
from main.utils_inference import gfp,gsp
#The class must be named Command, and subclass BaseCommand

mode_list = ['train','test']
ext_dict = {
	'0' : ['image','image-parse','pose'],
	'1' : ['cloth','cloth-mask']
}




class Command(BaseCommand):
	# Show this when the user types help
	help = "selected for bulk flush"
	
	# A command must define handle()
	def handle(self, *args, **options):
		mode = 'train'
		static_dir = settings.BASE_DIR + "/static/"
		data_dir = static_dir + "data/" 
		pairs_txt_file = data_dir + mode + '_pairs.txt'
		f = open(pairs_txt_file,'r')
		cnt = 0
		cnt_garment = 0
		cnt_pose = 0
		# for g in Garment.objects.all():
		# 	g.delete()
		# for p in Pose.objects.all():
		# 	p.delete()

		Garment.objects.all().delete()
		Pose.objects.all().delete()

		for cur_line in f.readlines():
			if 1:#cnt < 10:
					
				pk_cloth = cur_line.strip().split(" ")[1].split('.')[0].split("_")[0]
				cloth_path = gsp(gfp(static_dir,'data',mode,'cloth',pk_cloth))

				cloth_mask_path = gsp(gfp(static_dir,'data',mode,'cloth-mask',pk_cloth))
				existing_count = Garment.objects.filter(name=pk_cloth).count()
				if existing_count == 0:
					Garment(name=pk_cloth,cloth_path=cloth_path,cloth_mask_path=cloth_mask_path).save()
					cnt_garment+=1


				pk_image = cur_line.strip().split(" ")[0].split('.')[0].split("_")[0]
				image_path = gsp(gfp(static_dir,'data',mode,'image',pk_image))
				image_parse_path = gsp(gfp(static_dir,'data',mode,'image-parse',pk_image))
				pose_path = gsp(gfp(static_dir,'data',mode,'pose',pk_image))
				existing_count = Pose.objects.filter(name=pk_image).count()
				if existing_count == 0:
					Pose(name=pk_image,image_path=image_path,image_parse_path=image_parse_path,pose_path=pose_path).save()
					cnt_pose+=1
				cnt+=1
				print(cnt,pk_cloth,pk_image)

		print(cnt,cnt_garment,cnt_pose)




