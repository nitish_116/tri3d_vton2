from django.conf.urls import url
from . import views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

from django.conf.urls import url

app_name = 'main'
urlpatterns = [
	url(r'^$',views.index,name="index"),
	url(r'^goplist/$', views.goplist,name="goplist"),
	url(r'^ajax/GOP/$', views.fGOP,name="fGOP"),	
	url(r'^ajax/GOPFull/$', views.fGOPFull,name="fGOPFull"),	
]