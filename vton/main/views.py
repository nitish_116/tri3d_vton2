# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse,JsonResponse
from django.shortcuts import render,redirect,get_object_or_404
from django.template.context_processors import csrf
from pprint import pprint
import os,sys
import shutil
from main.models import Garment,Pose,GOP,Setting
from django.core.paginator import Paginator
import random
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

# 002875 Garment id , bad example of masking

#coding=utf-8
import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image
import argparse
from main.cp_dataset import CPDataset, CPDataLoader
from main.networks import GMM, UnetGenerator, load_checkpoint
from main.utils_inference import main,gsp,gfp
# Create your views here.

# Create your views here.
def index(request):
	cur_setting=None
	for cur_setting in Setting.objects.all():
		pass

	if not cur_setting:
		cur_setting = Setting()
		cur_setting.save()



	choice_count = 1

	if 'p' in request.GET.keys():
		pose_name = request.GET.get('p')
		poses = Pose.objects.filter(name=pose_name)
		if poses.count() != 1: 
			poses = Pose.objects.all().order_by('?')[:choice_count]
	else:
		poses = Pose.objects.all().order_by('?')[:choice_count]

	garments = Garment.objects.all().order_by('?')[:choice_count]
	C = {
		'pose' : poses[0],
		'garment' : garments[0],
		'is_running' : cur_setting.is_running,
	}
	C.update(csrf(request))
	return render(request,'index.html',C)


def goplist(request):
	C = {
		'gops' : GOP.objects.all()[::-1][:50]
	}
	C.update(csrf(request))
	return render(request,'gop_list.html',C)

@csrf_exempt
def fGOPFull(request):
	start_time = datetime.now()
	t1 = time.time()
	from binascii import a2b_base64
	cur_setting=None
	for cur_setting in Setting.objects.all():
		pass

	if not cur_setting:
		cur_setting = Setting()
		cur_setting.save()

	# cur_setting.is_running = False
	# cur_setting.save()

	print(cur_setting.is_running)
	if cur_setting.is_running:
		return JsonResponse({
			'message' : 'come back later, running ' + str(cur_setting),
		})

	cur_setting.is_running = True
	cur_setting.save()

	garment_name = request.POST.get('garment_name')
	pose_image_data = request.POST.get('pose_image_data')

	tmp_dir = settings.BASE_DIR + '/static/tmp/'
	if os.path.exists(tmp_dir):
		shutil.rmtree(tmp_dir)
	os.makedirs(tmp_dir)

	input_pose_path = tmp_dir + "/input_pose.jpg"
	if os.path.exists(input_pose_path):
		os.remove(input_pose_path)
	binary_data = a2b_base64(pose_image_data)
	fd = open(input_pose_path, 'wb')
	fd.write(binary_data)
	fd.close()

	#resize image
	from main.utils_inference import resize_image
	resize_image(input_pose_path)


	JPPNET_DIR = settings.BASE_DIR + "/../LIP/"
	py_file = JPPNET_DIR + 'evaluate_parsing_JPPNet-s2.py'
	jpp_input_path = JPPNET_DIR + 'datasets/examples/images/input_pose.jpg'
	jpp_output_path_parsing = JPPNET_DIR + 'output/parsing/val1/input_pose_vis.png'
	if os.path.exists(jpp_input_path):
		os.remove(jpp_input_path)
	shutil.copy(input_pose_path,jpp_input_path)

	import subprocess
	p = subprocess.Popen(["python",py_file], cwd=JPPNET_DIR)
	p.wait()

	#create keypoints json
	py_file = JPPNET_DIR + 'evaluate_pose_JPPNet-s2.py'
	jpp_output_path_pose = JPPNET_DIR + 'output/pose/val1/input_pose.txt'
	p = subprocess.Popen(["python",py_file], cwd=JPPNET_DIR)
	p.wait()

	parse_path = tmp_dir + "/pose_parse.png"
	keypoints_txt_path = tmp_dir + "/pose_keypoints.txt"
	keypoints_json_path = tmp_dir + "/pose_keypoints.json"
	shutil.copy(jpp_output_path_parsing,parse_path)
	shutil.copy(jpp_output_path_pose,keypoints_txt_path)

	f = open(keypoints_txt_path,'r')
	keypoints_str = f.read()
	f.close()

	import json
	sample_kp_path = settings.BASE_DIR + '/static/sample_kp.json'
	with open(sample_kp_path, "r") as read_file:
		sample_kp = json.load(read_file)
	
	sample_kp['people'][0]['pose_keypoints'] = []

	keypoints_list = keypoints_str.split(" ")
	point_dict = {}
	for i in range(16):
		px = int(keypoints_list[2*i])
		py = int(keypoints_list[2*i+1])
		point_dict[i] = (px,py)

		if i <=13:
			sample_kp['people'][0]['pose_keypoints'].append(px)
			sample_kp['people'][0]['pose_keypoints'].append(py)			
			sample_kp['people'][0]['pose_keypoints'].append(0)			

	for i in range(3):
		sample_kp['people'][0]['pose_keypoints'].append(point_dict[0][0])	
		sample_kp['people'][0]['pose_keypoints'].append(point_dict[0][1])
		sample_kp['people'][0]['pose_keypoints'].append(0)			

	sample_kp['people'][0]['pose_keypoints'].append(point_dict[15][0])
	sample_kp['people'][0]['pose_keypoints'].append(point_dict[15][1])
	sample_kp['people'][0]['pose_keypoints'].append(0)			

	index_json_string = json.dumps(sample_kp,indent=4)
	f = open(keypoints_json_path,'w')
	f.write(index_json_string)
	f.close()

		
	#register a pose id 
	static_dir = settings.BASE_DIR + "/static/"
	cur_pose = Pose()
	cur_pose.save()
	mode = 'train'

	cur_pose.name = "U"+str(cur_pose.pk)
	cur_pose.save()
	pk_image = cur_pose.name
	image_path = gfp(static_dir,'data',mode,'image',pk_image)
	image_parse_path = gfp(static_dir,'data',mode,'image-parse',pk_image)
	pose_path = gfp(static_dir,'data',mode,'pose',pk_image)

	
	shutil.copy(input_pose_path,image_path)
	shutil.copy(parse_path,image_parse_path)
	shutil.copy(keypoints_json_path,pose_path)

	Image.open(image_parse_path).convert('L').save(image_parse_path)

	cur_pose.image_path = gsp(image_path)
	cur_pose.image_parse_path = gsp(image_parse_path)
	cur_pose.pose_path = gsp(pose_path)
	cur_pose.save()

	#call main
	ans = main(garment_name,cur_pose.name)
	garment = Garment.objects.get(name=garment_name)
	pose = cur_pose
	gop = GOP(garment=garment,pose=pose)
	gop.save()

	gop_path = settings.BASE_DIR + '/static/gop/' + str(gop.pk) + "/"
	if not os.path.exists(gop_path):
		os.makedirs(gop_path)

	shutil.copy(ans['warp_cloth_path'],gop_path + "warp_cloth.jpg")
	shutil.copy(ans['output_path'],gop_path + "output.jpg")
	# shutil.rmtree(ans['dir'])

	# print(gop)
	# print(ans['warp_cloth_path'],gop_path+"warp_cloth.jpg")
	# print(ans['output_path'],gop_path + "output.jpg")


	end_time = datetime.now()
	gop.warp_cloth_path = gsp(gop_path + "warp_cloth.jpg")
	gop.tryon_path = gsp(gop_path + "output.jpg")
	gop.start_time = start_time
	gop.end_time = end_time
	gop.save()

	cur_setting.is_running = False
	cur_setting.save()

	t2=time.time()

	return JsonResponse({
		# 'message' : keypoints_str + " | time taken = " + str(t2-t1),
		'message' : 'success',
		'parse_path' : gsp(parse_path),
		'output_path' : gop.tryon_path,
		'warp_path' : gop.warp_cloth_path,
	})

	

def fGOP(request):
	start_time = datetime.now()
	cur_setting=None
	for cur_setting in Setting.objects.all():
		pass

	if not cur_setting:
		cur_setting = Setting()
		cur_setting.save()

	print(cur_setting.is_running)
	if 0:#cur_setting.is_running:
		return JsonResponse({
			'message' : 'come back later, running ' + str(cur_setting),
		})

	cur_setting.is_running = True
	cur_setting.save()
	
	
	garment_name = request.GET.get('garment_name')
	pose_name = request.GET.get('pose_name')

	cur_setting.pose = pose_name
	cur_setting.garment = garment_name
	cur_setting.save()

	print(garment_name,pose_name)
	ans = main(garment_name,pose_name)

	garment = Garment.objects.get(name=garment_name)
	pose = Pose.objects.get(name=pose_name)
	gop = GOP(garment=garment,pose=pose)
	gop.save()
	print(gop)

	gop_path = settings.BASE_DIR + '/static/gop/' + str(gop.pk) + "/"
	if not os.path.exists(gop_path):
		os.makedirs(gop_path)
		print(gop_path)

	pprint(ans)
	shutil.copy(ans['warp_cloth_path'],gop_path + "warp_cloth.jpg")
	shutil.copy(ans['output_path'],gop_path + "output.jpg")
	# shutil.rmtree(ans['dir'])

	print(gop)
	print(ans['warp_cloth_path'],gop_path+"warp_cloth.jpg")
	print(ans['output_path'],gop_path + "output.jpg")

	end_time = datetime.now()
	gop.warp_cloth_path = gsp(gop_path + "warp_cloth.jpg")
	gop.tryon_path = gsp(gop_path + "output.jpg")
	gop.start_time = start_time
	gop.end_time = end_time
	gop.save()

	cur_setting.is_running = False
	cur_setting.save()
	return JsonResponse({
		'message' : 'success',
		'output_path' : gop.tryon_path,
		'warp_path' : gop.warp_cloth_path,
	})
