# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from main.models import Garment,Pose,GOP,Setting
# Register your models here.

admin.site.register(Garment)
admin.site.register(Pose)
admin.site.register(GOP)
admin.site.register(Setting)
